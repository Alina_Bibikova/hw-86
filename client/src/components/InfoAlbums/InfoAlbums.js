import React, {Component} from 'react';
import {connect} from "react-redux";
import AlbumsListItems from "../AlbumsListItems/AlbumsListItems";
import {infoAlbums} from "../../store/actions/actionsArtist";

class InfoAlbums extends Component {
    componentDidMount() {
        this.props.infoAlbums(this.props.match.params.id)
    }

    render() {
        return (
            this.props.infoAlbum ? <div>
                <h2>Albums</h2>
                {this.props.infoAlbum.map(album => {
                    return (
                        <AlbumsListItems
                            key={album._id}
                            _id={album._id}
                            image={album.image}
                            description={album.description}
                            album={album.nameAlbums}
                            date={album.date}
                            name={album.nameArtist.name}
                        />
                    )
                })}
            </div> : null

        );
    }
}

const mapStateToProps = state => ({
    infoAlbum: state.albums.albums
});

const mapDispatchToProps = dispatch => ({
    infoAlbums: id => dispatch(infoAlbums(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(InfoAlbums);
import React from 'react';
import PropTypes from  'prop-types';

import {Card, CardBody} from "reactstrap";
import MusicThumbnail from "../MusicThumbnail/MusicThumbnail";
import {Link} from "react-router-dom";

const ArtistListItems = props => {
    return (
        <Card key={props._id} style={{marginTop: '10px'}}>
            <CardBody>
                <MusicThumbnail image={props.image}/>
                <Link to={'/infoAlbums/' + props._id}>
                    {props.name}
                </Link>
            </CardBody>
        </Card>
    );
};

ArtistListItems.propTypes = {
    image: PropTypes.string,
    _id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
};

export default ArtistListItems;
import React from 'react';
import {Card, CardBody} from "reactstrap";
import PropTypes from "prop-types";

const TracksListItems = props => {
    return (
        <Card onClick={props.click} key={props._id} style={{marginTop: '10px'}}>
            <CardBody>
                <p>Number: {props.trackNumber}</p>
                <p>Track: {props.nameTrack}</p>
                <p>longest: {props.longest}</p>
            </CardBody>
        </Card>
    );
};

TracksListItems.propTypes = {
    nameTrack: PropTypes.string,
    trackNumber: PropTypes.string,
    longest: PropTypes.string,
};

export default TracksListItems;

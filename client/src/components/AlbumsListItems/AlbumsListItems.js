import React from 'react';
import {Card, CardBody, CardText} from "reactstrap";
import MusicThumbnail from "../MusicThumbnail/MusicThumbnail";
import {Link} from "react-router-dom";
import PropTypes from "prop-types";

const AlbumsListItems = props => {
    return (
        <Card key={props._id} style={{marginTop: '10px'}}>
            <CardBody>
                <h4>Artist: {props.name}</h4>
                <MusicThumbnail image={props.image}/>
                <CardText>
                    <Link to={'/infoTrack/' + props._id}>
                        Album: {props.album}
                    </Link>
                </CardText>
                <CardText>
                    Date of an exit: {props.date} year
                </CardText>
                <CardText>
                    Description: {props.description} year
                </CardText>
            </CardBody>
        </Card>
    );
};

AlbumsListItems.propTypes = {
    image: PropTypes.string,
    _id: PropTypes.string.isRequired,
};

export default AlbumsListItems;
import React, {Component} from 'react';
import {connect} from "react-redux";
import {createTrackHistory, fetchTracks} from "../../store/actions/actionsAlbums";
import TracksListItems from "../../components/TracksListItems/TracksListItems";

class Tracks extends Component {
    componentDidMount() {
        this.props.onFetchTracks();
    }

    render() {
        return (
            this.props.tracks ? <div>
                <h2>Tracks</h2>
                {this.props.tracks.map(tracks => (
                    <TracksListItems
                        click={() => this.props.createTrackHistory(tracks._id)}
                        key={tracks._id}
                        trackNumber={tracks.trackNumber}
                        nameTrack={tracks.nameTrack}
                        longest={tracks.longest}
                    />
                ))}
            </div> : null
        );
    }
}

const mapStateToProps = state => ({
    tracks: state.albums.tracks
});

const mapDispatchToProps = dispatch => ({
    onFetchTracks: () => dispatch(fetchTracks()),
    createTrackHistory: TrackHistoryData => dispatch(createTrackHistory(TrackHistoryData))
});

export default connect(mapStateToProps, mapDispatchToProps)(Tracks);
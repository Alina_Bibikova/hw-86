import React, {Component, Fragment} from 'react';
import {Container} from "reactstrap";
import {Route, Switch, withRouter} from "react-router";
import Toolbar from "./components/UI/Toolbar/Toolbar";
import Artist from "./containers/Artist/Artist";
import InfoAlbums from "./components/InfoAlbums/InfoAlbums";
import InfoTracks from "./components/InfoTracks/InfoTracks";
import {connect} from "react-redux";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import Tracks from "./containers/Tracks/Tracks";
import TrackHistory from "./containers/TrackHistory/TrackHistory";

class App extends Component {
  render() {
    return (
        <Fragment>
            <header>
                <Toolbar user={this.props.user}/>
            </header>
            <Container style={{marginTop: '20px'}}>
                <Switch>
                    <Route path="/" exact component={Artist}/>
                    <Route path="/infoAlbums/:id" exact component={InfoAlbums}/>
                    <Route path="/infoTrack/:id" exact component={InfoTracks}/>
                    <Route path="/register" exact component={Register} />
                    <Route path="/login" exact component={Login} />
                    <Route path="/tracks" exact component={Tracks} />
                    <Route path="/track_history" exact component={TrackHistory} />
                </Switch>
            </Container>
        </Fragment>
    );
  }
}

const mapStateToProps = state => ({
    user: state.users.user
});

export default withRouter(connect(mapStateToProps)(App));

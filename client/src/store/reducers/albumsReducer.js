import {
    CREATE_TRACK_HISTORY_FAILURE,
    FETCH_TRACK_HISTORY_SUCCESS,
    FETCH_TRACK_FAILURE,
    FETCH_TRACK_SUCCESS,
    FETCH_TRACKS_FAILURE,
    INFO_TRACKS_SUCCESS
} from "../actions/actionsAlbums";
import {FETCH_ALBUMS_SUCCESS} from "../actions/actionsArtist";

const initialState = {
    albums: [],
    tracks: [],
    trackHistory: null,
    tracksError: null,
    infoTracks: null,
    error: null,
    trackHistoryError: null
};

const albumsReducer = (state = initialState, action) => {
    switch (action.type) {

        case FETCH_ALBUMS_SUCCESS:
            return {
                ...state,
                albums: action.data
            };

        case INFO_TRACKS_SUCCESS:
            return {
                ...state,
                infoTracks: action.data
            };

        case FETCH_TRACKS_FAILURE:
            return {
                ...state,
                error: action.error
            };

        case FETCH_TRACK_SUCCESS:
            return {
                ...state,
                tracks: action.tracks,
                tracksError: null
            };

        case FETCH_TRACK_FAILURE:
            return {
                ...state,
                tracksError: action.error
            };

        case FETCH_TRACK_HISTORY_SUCCESS:
            return {
                ...state,
                trackHistory: action.trackHistory,
                trackHistoryError: null
            };

        case CREATE_TRACK_HISTORY_FAILURE:
            return {
                ...state,
                trackHistoryError: action.error
            };

        default:
            return state;
    }
};

export default albumsReducer;
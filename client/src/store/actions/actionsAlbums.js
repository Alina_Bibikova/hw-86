import axios from '../../axios-api';
import {push} from "connected-react-router";

export const INFO_TRACKS_SUCCESS = 'INFO_TRACKS_SUCCESS';
export const FETCH_TRACKS_FAILURE = 'FETCH_TRACKS_FAILURE';
export const FETCH_TRACK_SUCCESS = 'FETCH_TRACK_SUCCESS';
export const FETCH_TRACK_FAILURE = 'FETCH_TRACK_FAILURE';
export const FETCH_TRACK_HISTORY_SUCCESS = 'FETCH_TRACK_HISTORY_SUCCESS';
export const FETCH_TRACK_HISTORY_FAILURE = 'FETCH_TRACK_HISTORY_FAILURE';
export const CREATE_TRACK_HISTORY_SUCCESS = 'CREATE_TRACK_HISTORY_SUCCESS';
export const CREATE_TRACK_HISTORY_FAILURE = 'CREATE_TRACK_HISTORY_FAILURE';

export const fetchTrackSuccess = tracks => ({type: FETCH_TRACK_SUCCESS, tracks});
export const fetchTrackFailure = error => ({type: FETCH_TRACK_FAILURE, error});
export const infoTracksSuccess = data => ({type: INFO_TRACKS_SUCCESS, data});
export const fetchTracksFailure = error => ({type: FETCH_TRACKS_FAILURE, error});
export const fetchTrackHistorySuccess = trackHistory => ({type: FETCH_TRACK_HISTORY_SUCCESS, trackHistory});
export const fetchTrackHistoryFailure = error => ({type: FETCH_TRACK_HISTORY_FAILURE, error});
export const createTrackHistorySuccess = () => ({type: CREATE_TRACK_HISTORY_SUCCESS});
export const createTrackHistoryFailure = error => ({type: CREATE_TRACK_HISTORY_FAILURE, error});

export const fetchTrackHistory = () => {
    return (dispatch, getState) => {
        const token = getState().users.user;
        if (!token) {
            dispatch(push('/login'))
        } else {
            return axios.get('/track_history', {headers: {'Authorization': token.token}}).then(
                response => dispatch(fetchTrackHistorySuccess(response.data))
            ).catch(error => dispatch(fetchTrackHistoryFailure(error)));
        }
    };
};

export const createTrackHistory = TrackHistoryData => {
    return (dispatch, getState) => {
        const token = getState().users.user.token;
        if (!token) {
            dispatch(push('/login'));
        } else {
            return axios.post('/track_history', {track: TrackHistoryData}, {headers: {'Authorization': token}}).then(
                () => dispatch(createTrackHistorySuccess())
            ).catch(error => dispatch(createTrackHistoryFailure(error)));
        }
    };
};

export const infoTrack = id => {
    return dispatch => {
        return axios.get(`/tracks?albums=${id}`).then(
            response => dispatch(infoTracksSuccess(response.data))
        ).catch(error => dispatch(fetchTracksFailure(error)));
    };
};

export const fetchTracks = () => {
    return dispatch => {
        return axios.get('/tracks').then(
            response => dispatch(fetchTrackSuccess(response.data))
        ).catch(error => dispatch(fetchTrackFailure(error)));
    };
};


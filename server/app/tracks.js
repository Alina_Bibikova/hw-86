const express = require('express');
const Tracks = require('../models/Track');

const router = express.Router();

router.get('/', (req, res) => {
    if (req.query.albums) {
        Tracks.find({nameAlbum: req.query.albums}).sort('trackNumber').populate('nameAlbum')
            .then(result => {res.send(result)})
            .catch(() => res.sendStatus(500));
    } else {
        Tracks.find().populate({path: 'nameAlbum', populate: {path: 'nameArtist', model: 'Artist'}})
            .then(track => res.send(track))
            .catch(() => res.sendStatus(500));
    }
});

router.get('/:id', (req, res) => {
    Tracks.findById(req.params.id).populate('nameAlbum')
        .then(result => {
            if (result) return res.send(result);
            res.sendStatus(404)})
        .catch(() => res.sendStatus(500));
});

router.post('/', (req, res) => {
    const trackData = req.body;
    const track = new Tracks(trackData);

    track.save()
        .then(result => res.send(result))
        .catch(error => res.status(400).send(error))
});

router.put('/:id', async (req, res) => {
    try {
        const updateTrack = await Tracks.findById(req.params.id);
        updateTrack.nameTrack = req.body.nameTrack;
        updateTrack.longest = req.body.longest;
        updateTrack.nameAlbum = req.body.nameAlbum;
        updateTrack.trackNumber = req.body.trackNumber;

        await updateTrack.save();
        return res.send(updateTrack);
    } catch (error) {
        return res.status(400).send(error)
    }
});

router.delete('/:id', async (req, res) => {
    try {
        await Tracks.deleteOne({_id: req.params.id});
        return res.sendStatus(200);
    } catch (error) {
        return res.status(400).send(error);
    }
});

module.exports = router;